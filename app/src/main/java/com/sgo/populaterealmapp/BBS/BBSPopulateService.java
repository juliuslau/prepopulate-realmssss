package com.sgo.populaterealmapp.BBS;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sgo.populaterealmapp.BBS.Beans.BBSBankModel;
import com.sgo.populaterealmapp.BBS.Beans.BBSCommModel;
import com.sgo.populaterealmapp.Biller.DateTimeFormat;
import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.WebParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

/**
 * Created by yuddistirakiki on 5/10/16.
 */
public class BBSPopulateService extends IntentService {

    public static final int DOWNLOAD_ERROR = 10;
    public static final int DOWNLOAD_SUCCESS = 11;
    public static final int DOWNLOAD_PROGRESS = 12;
    private final int SCHEME_CTA = 0;
    private final int SCHEME_ATC = 1;

    JSONArray cta;
    JSONArray atc;
    JSONObject bankCTA;
    JSONObject bankATC;

    ResultReceiver receiver;
    Realm realm;
    int appType;
    Bundle oi;
    File folder;

    @RealmModule(classes = { BBSBankModel.class, BBSCommModel.class})
    private static class BBSModule {
    }

    public BBSPopulateService() {
        super("BBSPopulateService");
    }

    public BBSPopulateService(String name) {
        super(name);
    }

    private void createFolder(){
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "PopulateRealmApp");
        if (!folder.exists()) {
            folder.mkdir();
        }

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        createFolder();

        appType = intent.getIntExtra("appType",0);
        oi = new Bundle();

        RealmConfiguration config = new RealmConfiguration.Builder()
                .directory(folder)
                .name(intent.getStringExtra("realmName"))
                .schemaVersion(intent.getIntExtra("realVer",0))
                .modules(new BBSModule())
                .migration(new BBSRealMigration())
                .build();

        realm = Realm.getInstance(config);

        receiver = intent.getParcelableExtra("receiver");

        cta = new JSONArray();
        atc = new JSONArray();
        getBBSCom("CTA");
    }

    public void getBBSCom(final String schemeCode){
        try{
            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.COMM_ID_REMARK, MyApiClient.COMM_ID);
            params.put(WebParams.SCHEME_CODE,schemeCode);
            Log.d("params list community",params.toString());
            MyApiClient.getListCommunity(this, true,params,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response get BBlistComm:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            JSONArray community = response.getJSONArray("community");
                            if(community != null && community.length() != 0 ){
                                if(schemeCode.equalsIgnoreCase("CTA")){
                                    cta = community;
                                    getBBSCom("ATC");
                                }
                                else {
                                    atc = community;
                                    if(cta != null && cta.length() > 0)
                                        getCTA();
                                    else
                                        InsertData();
                                }
                            }
                            else {
                                if(schemeCode.equalsIgnoreCase("CTA")) {
                                    oi.putString("msg", "There's no data CTA");
                                    oi.putBoolean("isContinue", true);
                                    EndError(DOWNLOAD_ERROR, oi);
                                    getBBSCom("ATC");
                                }
                                else {
                                    if(cta.length() > 0){
                                        getCTA();
                                    }
                                    else {
                                        oi.putString("msg", "There's no data");
                                        EndError(DOWNLOAD_ERROR, oi);
                                    }
                                }
                            }
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        oi.putString("msg","Exception Json");
                        EndError(DOWNLOAD_ERROR,oi);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi biller data list buy:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
            oi.putString("msg","Exception Json");
            EndError(DOWNLOAD_ERROR,oi);
        }
    }

    public void getCTA(){
        try{
            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.COMM_CODE, cta.getJSONObject(0).getString(WebParams.COMM_CODE));
            Log.d("params getCTA",params.toString());
            MyApiClient.getGlobalBBSBankC2A(this, true,params,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response getCTA:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            bankCTA = response;
                            InsertData();
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        oi.putString("msg","Exception Json");
                        EndError(DOWNLOAD_ERROR,oi);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi getCTA:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
            oi.putString("msg","Exception Json");
            EndError(DOWNLOAD_ERROR,oi);
        }
    }

    public void getATC(){
        try{
            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.COMM_CODE, atc.getJSONObject(0).getString(WebParams.COMM_CODE));
            params.put(WebParams.MEMBER_CODE, atc.getJSONObject(0).getString(WebParams.MEMBER_CODE));
            Log.d("params getATC",params.toString());
            MyApiClient.getGlobalBBSBankA2C(this, true,params,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response getATC:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            bankATC = response;
                            InsertData();
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        oi.putString("msg","Exception Json");
                        EndError(DOWNLOAD_ERROR,oi);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi getATC:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
            oi.putString("msg","Exception Json");
            EndError(DOWNLOAD_ERROR,oi);
        }
    }




    private void InsertData(){
        receiver.send(DOWNLOAD_PROGRESS,null);
        final String curr_date = DateTimeFormat.getCurrentDate();

        realm.beginTransaction();

        try {
            receiver.send(DOWNLOAD_PROGRESS,null);
            int i;
            //clear data table BBSCommModel
            realm.delete(BBSCommModel.class);

            //prepare temp variable to insert to BBSCommModel
            BBSCommModel tempBBScomm;

            //insert table BBSCommModel dari data JSON CTA
            for ( i= 0; i < cta.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                tempBBScomm = realm.createOrUpdateObjectFromJson(BBSCommModel.class, cta.getJSONObject(i));
                tempBBScomm.setScheme_code("CTA");
                tempBBScomm.setLast_update(curr_date);
            }

            //insert table BBSCommModel dari data JSON ATC
            for ( i= 0; i < atc.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                tempBBScomm = realm.createOrUpdateObjectFromJson(BBSCommModel.class, atc.getJSONObject(i));
                tempBBScomm.setScheme_code("ATC");
                tempBBScomm.setLast_update(curr_date);
            }

            //clear table BBSBankModel
            realm.delete(BBSBankModel.class);

            //prepare temp variable to insert to BBSBankModel
            BBSBankModel tempBBSbank;

            //prepare variable data yang dibutuhkan
            JSONArray commSource = new JSONArray();
            JSONArray commBenef = new JSONArray();
            String commID;
            String schemeCode;

            schemeCode = "CTA";
            BBSCommModel bbsCommModel = realm.where(BBSCommModel.class).equalTo("scheme_code",schemeCode).findFirst();
            commID = bbsCommModel.getComm_id();
            commSource = bankCTA.optJSONArray(WebParams.COMM_SOURCE);
            commBenef = bankCTA.optJSONArray(WebParams.COMM_BENEF);

            if(commSource == null)
                commSource = new JSONArray();
            if(commBenef == null)
                commBenef = new JSONArray();

            //insert table BBSBankModel source dari data JSON CTA
            for ( i= 0; i < commSource.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                tempBBSbank = realm.createObjectFromJson(BBSBankModel.class, commSource.getJSONObject(i));
                tempBBSbank.setComm_type("SOURCE");
                tempBBSbank.setComm_id(commID);
                tempBBSbank.setScheme_code(schemeCode);
                tempBBSbank.setLast_update(curr_date);
            }

            for ( i= 0; i < commBenef.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                tempBBSbank = realm.createObjectFromJson(BBSBankModel.class, commBenef.getJSONObject(i));
                tempBBSbank.setComm_type("BENEF");
                tempBBSbank.setComm_id(commID);
                tempBBSbank.setScheme_code(schemeCode);
                tempBBSbank.setLast_update(curr_date);
            }

            realm.commitTransaction();

            oi.putInt("appType",appType);
            EndError(DOWNLOAD_SUCCESS,oi);
        }
        catch (JSONException e){
            Log.e("error",e.getMessage());
            realm.cancelTransaction();
            oi.putString("msg",e.getMessage());
            EndError(DOWNLOAD_ERROR,oi);
        }
    }




    private void EndRealm(){
        if(realm.isInTransaction())
            realm.cancelTransaction();

        if(realm != null && !realm.isClosed())
            realm.close();
    }

    private void EndError(int ReceiverTag, Bundle io){
        Bundle buk = (Bundle)io.clone();
        io.clear();
        EndRealm();
        receiver.send(ReceiverTag,buk);

    }
}
