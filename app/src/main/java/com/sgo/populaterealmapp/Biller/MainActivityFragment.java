package com.sgo.populaterealmapp.Biller;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.R;
import com.sgo.populaterealmapp.ServiceReceiver;

/**
 * Fragment yg diimplemen di MainActivity
 * berisi layout/UI untuk membuat realm
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener, ServiceReceiver.Receiver {

    private static final int PERMISSIONS_REQ_WRITEEXTERNALSTORAGE = 131;

    /**
     * Custom AppType biar nanti di service gak salah nentuin app mana yang sedang diproses
     */
    public static final int SCASH = 0x11;
    public static final int AKD = 0x13;
    public static final int UNIK = 0x12;
    public static final int KCB = 0x14;
    public static final int HPMU = 0x15;
    public static final int HPKU = 0x16;

    /**
     * version realm production
     * pastikan sama dengan di project
     */
    public static final int SCASHVerPROD = 0;
    public static final int AKDVerPROD = 0;
    public static final int UNIKVerPROD = 0;
    public static final int KCBVerPROD = 0;
    public static final int MYCASHVerPROD = 0;
    public static final int HPKUVerPROD = 1;

    /**
     * version realm development
     * pastikan sama dengan di project
     */
    public static final int SCASHVerDEV = 0;
    public static final int AKDVerDEV = 0;
    public static final int UNIKVerDEV = 1;
    public static final int KCBVerDEV = 0;
    public static final int MYCASHVerDev = 0;
    public static final int HPKUVerDev = 1;


    AlertDialog.Builder alertbox;
    ServiceReceiver resultReceiever;
    int button_id;
    ProgressBar mProgBar;
    TextView txt_pin;
    TextView txt_akd;
    TextView txt_unik;
    TextView txt_kcb;
    TextView txt_mycash;
    TextView txt_hpku;
    Switch sw_prod;
    int mProgress;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_main, container, false);
        v.findViewById(R.id.button_pin).setOnClickListener(this);
        v.findViewById(R.id.button_akardaya).setOnClickListener(this);
        v.findViewById(R.id.button_unik).setOnClickListener(this);
        v.findViewById(R.id.button_KCB).setOnClickListener(this);
        v.findViewById(R.id.button_MYCASH).setOnClickListener(this);
        v.findViewById(R.id.button_HPKU).setOnClickListener(this);
        mProgBar = (ProgressBar)v.findViewById(R.id.progressBar);
        txt_pin = (TextView)v.findViewById(R.id.textView_pin);
        txt_akd = (TextView)v.findViewById(R.id.textView_akd);
        txt_unik = (TextView)v.findViewById(R.id.textView_unik);
        txt_kcb = (TextView)v.findViewById(R.id.textView_KCB);
        txt_mycash = (TextView)v.findViewById(R.id.textView_MYCASH);
        txt_hpku = (TextView)v.findViewById(R.id.textView_HPKU);

        //inisialisasi switch yang nentuin dev atau production di kiri bawah
        sw_prod = (Switch) v.findViewById(R.id.switch1);
        sw_prod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MyApiClient.IS_PROD = isChecked;
                if(MyApiClient.IS_PROD)
                    MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
                else
                    MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;
                MyApiClient.initializeAddress();
                Log.d("isi ISPROD", String.valueOf(MyApiClient.IS_PROD));
            }
        });
        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        resultReceiever = new ServiceReceiver(new Handler());
        resultReceiever.setReceiver(this);

        mProgress = 0;

        if(MyApiClient.IS_PROD)
            MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
        else
            MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;

        //tiap button diklik akan keluar dialog ini
        alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setTitle("Populate Realm");
        alertbox.setMessage("Apakah yakin membuat baru File Realm ini ?");
        alertbox.setPositiveButton("Ok", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        mProgBar.setVisibility(View.VISIBLE);
                        mProgress = 0;
                        mProgBar.setProgress(mProgress);
                        Intent startIntent = new Intent(getActivity(),
                                PopulateService.class);

                        //isi intent dengan data sesuai dengan aplikasi yg dipilih
                        //berdasarkan button_id nya
                        // datanya berupa comm_id, path webservicenya, realm version dan realm namenya
                        switch (button_id){
                            case R.id.button_pin :
                                txt_pin.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_PIN;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.SCASH;
                                    startIntent.putExtra("realmName", "pinrealmdev.realm");
                                    startIntent.putExtra("realmVer",SCASHVerDEV);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_PIN;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.SCASH;
                                    startIntent.putExtra("realmName", "pinrealm.realm");
                                    startIntent.putExtra("realmVer",SCASHVerPROD);
                                }

                                startIntent.putExtra("appType",SCASH);
                                break;
                            case R.id.button_akardaya:
                                txt_akd.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_AKD;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.AKD;
                                    startIntent.putExtra("realmName", "akdrealmdev.realm");
                                    startIntent.putExtra("realmVer",AKDVerDEV);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_AKD;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.AKD2;

                                    startIntent.putExtra("realmName", "akdrealm.realm");
                                    startIntent.putExtra("realmVer",AKDVerPROD);
                                }

                                startIntent.putExtra("appType",AKD);
                                break;
                            case R.id.button_unik:
                                txt_unik.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_UNIK;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.UNIK;
                                    startIntent.putExtra("realmName", "unikrealmdev.realm");
                                    startIntent.putExtra("realmVer",UNIKVerDEV);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_UNIK;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.UNIK;
                                    startIntent.putExtra("realmName", "unikrealm.realm");
                                    startIntent.putExtra("realmVer",UNIKVerPROD);
                                }

                                startIntent.putExtra("appType",UNIK);
                                break;
                            case R.id.button_KCB:
                                txt_kcb.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_KCB;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.KCB;
                                    startIntent.putExtra("realmName", "kmcbrealmdev.realm");
                                    startIntent.putExtra("realmVer",KCBVerDEV);

                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_KCB;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.KCB;
                                    startIntent.putExtra("realmName", "kmcbrealm.realm");
                                    startIntent.putExtra("realmVer",KCBVerPROD);
                                }

                                startIntent.putExtra("appType",KCB);
                                break;
                            case R.id.button_MYCASH:
                                txt_mycash.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.setMyCashHeader();
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_MYCASH;
                                    MyApiClient.address = MyApiClient.headaddressDEVMycash+MyApiClient.MYCASH;
                                    startIntent.putExtra("realmName", "mycashdevbiller.realm");
                                    startIntent.putExtra("realmVer", MYCASHVerDev);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_MYCASH;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.MYCASH;
                                    startIntent.putExtra("realmName", "mycashbiller.realm");
                                    startIntent.putExtra("realmVer", MYCASHVerPROD);
                                }

                                startIntent.putExtra("appType",HPMU);
                                break;
                            case R.id.button_HPKU:
                                txt_hpku.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_HPKU;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.HPKU;
                                    startIntent.putExtra("realmName", "saldomudevrealm.realm");
                                    startIntent.putExtra("realmVer",HPKUVerDev);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_HPKU;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.HPKU;
                                    startIntent.putExtra("realmName", "saldomurealm.realm");
                                    startIntent.putExtra("realmVer",HPKUVerPROD);
                                }

                                startIntent.putExtra("appType",HPKU);
                                break;
                        }
                        MyApiClient.initializeAddress();
                        createTheRealm(startIntent);
                    }
                });
        alertbox.setNegativeButton("Cancel", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {}
                });



        mProgBar.setMax(100);
        mProgBar.setProgress(0);
        mProgBar.setIndeterminate(false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQ_WRITEEXTERNALSTORAGE);
        }
    }

    @Override
    public void onClick(View v) {
        button_id = v.getId();
        showDialog();

    }


    private void showDialog(){
        if(alertbox != null)
            alertbox.show();
    }

    private void createTheRealm(Intent startIntent){
        startIntent.putExtra("receiver", resultReceiever);
        getActivity().startService(startIntent);
    }

    /**
     * Interface onReceiveResult yang ditrigger dari service PopulateService.java
     * isinya nentuin UI jika dapet result yg error , sukses atau sedang on progress
     * @param resultCode
     * @param resultData
     */
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case PopulateService.DOWNLOAD_ERROR:
                Toast.makeText(getActivity(),resultData.getString("msg",""),Toast.LENGTH_SHORT).show();
                mProgBar.setVisibility(View.GONE);
                mProgBar.setProgress(0);
                break;
            case PopulateService.DOWNLOAD_SUCCESS:
                Toast.makeText(getActivity(),"Realm file selesai",Toast.LENGTH_SHORT).show();
                mProgBar.setProgress(mProgress+100);
                switch (resultData.getInt("appType")){
                    case SCASH :
                        txt_pin.setVisibility(View.VISIBLE);
                        break;
                    case AKD   :
                        txt_akd.setVisibility(View.VISIBLE);
                        break;
                    case UNIK  :
                        txt_unik.setVisibility(View.VISIBLE);
                        break;
                    case HPMU  :
                        txt_mycash.setVisibility(View.VISIBLE);
                        break;
                    case HPKU  :
                        txt_hpku.setVisibility(View.VISIBLE);
                        break;
                }
                break;
            case PopulateService.DOWNLOAD_PROGRESS:
                if(mProgBar.getProgress() <= 98)
                    mProgBar.setProgress(++mProgress);
                break;
        }
    }

}
