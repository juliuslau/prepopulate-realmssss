package com.sgo.populaterealmapp;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;

/*
  Created by Administrator on 8/15/2014.
 */
public class CoreApp extends Application {

    private String realmName = "";
    private int realmVersion = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

        Realm.init(this);

//        copyBundledRealmFile(CoreApp.this.getResources().openRawResource(R.raw.scashdev), realmName);

//        RealmConfiguration config = new RealmConfiguration.Builder(new File(Environment.getExternalStorageDirectory(),"PopulateRealmApp"))
//                .name(realmName)
//                .schemaVersion(realmVersion)
//                .migration(new CustomRealMigration())
//                .build();
//
//
//
//        Realm.setDefaultConfiguration(config);

        MyApiClient.initialize(this);

    }

}
