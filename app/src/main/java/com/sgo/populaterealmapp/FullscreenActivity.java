package com.sgo.populaterealmapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.sgo.populaterealmapp.Biller.MainActivity;
import com.sgo.populaterealmapp.Pertamina.PertaminaActivity;

/**
 * Activity pertama kali yang di buka, menampilkan button pilihan untuk memilih tipe
 * realm yang mau dibuat
 */
public class FullscreenActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_SIGN_IN = 100;
    private GoogleSignInClient googleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
//        googleSignInClient = buildGoogleSignInClient();
//        startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }

//    private GoogleSignInClient buildGoogleSignInClient() {
//        GoogleSignInOptions signInOptions =
//                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                        .requestScopes(Drive.SCOPE_FILE)
//                        .build();
//        return GoogleSignIn.getClient(this, signInOptions);
//    }


    /**
     * Buka activity untuk membuat realm biller
     * @param view
     */
    public void bukabiller(View view) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /**
     * Buka activity untuk membuat realm pertamina
     * @param view
     */
    public void bukapertamina(View view) {
        Intent i = new Intent(this, PertaminaActivity.class);
        startActivity(i);
    }

    /**
     * buka activity untuk membuat realm bbs
     * (tapi udah gak digunakan karena datanya berbeda tiap user)
     * @param view
     */
    public void bukabbs(View view) {
        Intent i = new Intent(this, com.sgo.populaterealmapp.BBS.MainActivity.class);
        startActivity(i);
    }

    /**
     * buka activity untuk membuat realm bbs member(punya ko jul)
     * @param view
     */
    public void bukabbsmember(View view) {
        Intent i = new Intent(this, com.sgo.populaterealmapp.BBSMember.MainActivity.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_SIGN_IN:
                Log.i("fullscreenactivity", "Sign in request code");
                // Called after user is signed in.
                if (resultCode == RESULT_OK) {
                    Log.i("fullscreenactivity", "Signed in successfully.");
                    Toast.makeText(this,"Login Google Drive Berhasil",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
