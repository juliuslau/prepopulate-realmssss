package com.sgo.populaterealmapp;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.sgo.populaterealmapp.Biller.DateTimeFormat;

import org.apache.commons.codec.binary.Base64;

import java.util.StringTokenizer;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class API untuk hit webservice yang diinginkan
 * sama seperti MyApiClient di project PIN lainnya
 */
public class MyApiClient {

    private static MyApiClient singleton = null;
    private Context mContext;

    public MyApiClient(){

    }
    public MyApiClient(Context _context){
        this.setmContext(_context);
    }

    public static MyApiClient getInstance( ) {
        return singleton;
    }


    /**
     * inisialisasi loopj untuk siapin client API dengan header dll
     * @param _context
     */
    public static void initialize(Context _context) {
        if(singleton == null) {
            singleton = new MyApiClient(_context);
            singleton.asyncHttpClient=new AsyncHttpClient();
            singleton.asyncHttpClient.addHeader("Authorization", "Basic " + getBasicAuth());
            singleton.syncHttpClient=new SyncHttpClient();
            singleton.syncHttpClient.addHeader("Authorization", "Basic " + getBasicAuth());
        }
    }

    public static Boolean IS_PROD = false;
    public static String COMM_ID;


    /**
     * headaddress dev dan prod
     * ada juga path aplikasinya
     */
//    public static final String headaddressDEV = "http://116.90.162.173:59088/";
    public static final String headaddressDEV = "https://mobile-dev.espay.id/";
    public static final String headaddressDEVMycash = "http://192.168.88.22:60013/";
    public static final String headaddressPROD = "https://mobile.espay.id/";
    public static final String SCASH = "scash";
    public static final String UNIK = "edik2";
    public static final String AKD = "akardaya";
    public static final String AKD2 = "akardaya2";
    public static final String KCB = "kmbc";
    public static final String MYCASH = "mycash";
    public static final String HPKU = "hpku";
    public static String APP_ID ;
    public static String address;
    public static String headaddressfinal;
    public static String USER_ID;

    //Link webservices Signature


    public static String LINK_GET_BILLER_TYPE;
    public static String LINK_LIST_BILLER;
    public static String LINK_DENOM_RETAIL;
    public static String LINK_LIST_BANK_BILLER;
    public static String LINK_COMM_ACCOUNT_COLLECTION;
    public static String LINK_GLOBAL_BBS_COMM;
    public static String LINK_GLOBAL_BBS_BANK_C2A;
    public static String LINK_GLOBAL_BBS_BANK_A2C;
    public static String LINK_GLOBAL_BBS_ALL_COMM;
    public static String LINK_LISTCOMMUNITY_BBS;
    public static String LINK_BANK_PRODUCT_LIST;

    /**
     * list path webservicenya
     */
    public static void initializeAddress(){

        // WebService BIller
        LINK_COMM_ACCOUNT_COLLECTION    = address + "/CommAcct/Retrieve";
        LINK_GET_BILLER_TYPE            = address + "/ServiceBillerType/getBillerType";
        LINK_LIST_BILLER                = address + "/BillerEspay/Retrieve";
        LINK_DENOM_RETAIL               = address + "/DenomBiller/Retrieve";
        LINK_LIST_BANK_BILLER           = address + "/BankBiller/Retrieve";
        LINK_GLOBAL_BBS_COMM = address + "/GlobalBBSComm/Retrieve";
        LINK_GLOBAL_BBS_BANK_C2A = address + "/GlobalBBSBankC2A/Retrieve";
        LINK_GLOBAL_BBS_BANK_A2C = address + "/GlobalBBSBankA2C/Retrieve";
        LINK_GLOBAL_BBS_ALL_COMM = address + "/GlobalBBSAllComm/Retrieve";
        LINK_LISTCOMMUNITY_BBS = address + "/ListCommunity/Retrieve";
        LINK_BANK_PRODUCT_LIST = address + "/Community/Bankproductlist";
    }


    //-----------------------------------------------------------------------------------------------------------------




    public static final int TIMEOUT = 120000; // 200 x 1000 = 3 menit

    /**
     * comm id jika dibutuhkan, ada dev dan prod
     */
    public static String COMMID_DEV_PIN = "SGOEMONEY14153473007IMAM";
    public static String COMMID_PROD_PIN = "SGOANCHORE1424964699RMTHB";  //prod

    public static String COMMID_DEV_AKD = "EMONEYMAKA1458297012HV4Q3"; //dev
    public static String COMMID_PROD_AKD = "EMONEYMAKA1429005701H921A";  //prod

    public static String COMMID_DEV_UNIK = "EMOEDIKK21451880774SHE7L";
    public static String COMMID_PROD_UNIK = "EMOEDIKK1450692888Y4AYU";  //prod

    public static String COMMID_DEV_KCB = "EMOKBC1465201866G27V3";
    public static String COMMID_PROD_KCB = "";  //prod

    public static String COMMID_DEV_MYCASH = "MYCASH14997746915ZTVT";
    public static String COMMID_PROD_MYCASH = "EMOHAPEMU1476368280QN5SH";  //prod

    public static String COMMID_DEV_HPKU = "EMOSALDOMU1500439694RS6DD";  //prod
    public static String COMMID_PROD_HPKU = "SALDOMU1503988580RFVBK";  //prod

    public static String APPID_PIN = "PIN";
    public static String APPID_UNIK = "EDIK2";
    public static String APPID_AKD = "AKD";

    private AsyncHttpClient asyncHttpClient;
    private AsyncHttpClient syncHttpClient;

    public static UUID getUUID(){
        return UUID.randomUUID();
    }

    public static String getWebserviceName(String link){
        StringTokenizer tokens = new StringTokenizer(link, "/");
        int index = 0;
        while(index<3) {
            tokens.nextToken();
            index++;
        }
        return tokens.nextToken();
    }

    private static String getBasicAuth() {
        String stringEncode = "dev.api.mobile"+":"+"590@dev.api.mobile!";
        byte[] encodeByte = Base64.encodeBase64(stringEncode.getBytes());
        String encode = new String(encodeByte);
        return encode.replace('+','-').replace('/','_');
    }

    private static String getBasicAuthMyCash() {
        String stringEncode = "mycashmobile"+":"+"mycashmobile";
        byte[] encodeByte = Base64.encodeBase64(stringEncode.getBytes());
        String encode = new String(encodeByte);
        return encode.replace('+','-').replace('/','_');
    }


    public static RequestParams getSignatureWithParams(String commID, String linknya, String user_id,String access_key){

        String webServiceName = getWebserviceName(linknya);
        UUID uuidnya = getUUID();
        String dtime = DateTimeFormat.getCurrentDateTime();
        String msgnya = uuidnya+dtime+APP_ID+webServiceName+ commID + user_id;

        String hash = null;
        Mac sha256_HMAC;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(access_key.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hmacData = sha256_HMAC.doFinal(msgnya.getBytes("UTF-8"));

            hash = new String(encodeUrlSafe(hmacData));

        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.put(WebParams.RC_UUID, uuidnya);
        params.put(WebParams.RC_DTIME, dtime);
        params.put(WebParams.SIGNATURE, hash);

        return params;
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

    public static void get(Context mContext,String url, AsyncHttpResponseHandler responseHandler, Boolean isSync) {
        if(IS_PROD)getClient(isSync).setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        getClient(isSync).setURLEncodingEnabled(true);
        getClient(isSync).get(getInstance().getmContext(), url, responseHandler);
    }

    public static void post(Context mContext,String url, RequestParams params, AsyncHttpResponseHandler responseHandler,Boolean isSync) {
        if(IS_PROD)getClient(isSync).setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        getClient(isSync).post(mContext, url, params, responseHandler);
    }

    public static AsyncHttpClient getClient(Boolean isSync)
    {
        // Return the synchronous HTTP client when the thread is not prepared

        if (Looper.myLooper() == null || isSync ) {
            getInstance().syncHttpClient.setTimeout(TIMEOUT);
            getInstance().syncHttpClient.setMaxRetriesAndTimeout(2, 10000);
            return getInstance().syncHttpClient;
        }
        getInstance().asyncHttpClient.setTimeout(TIMEOUT);
        getInstance().asyncHttpClient.setMaxRetriesAndTimeout(2, 10000);

        return getInstance().asyncHttpClient;
    }

    public static void setMyCashHeader(){
        getClient(true).removeAllHeaders();
        getClient(true).addHeader("Authorization", "Basic " + getBasicAuthMyCash());

        getClient(false).removeAllHeaders();
        getClient(false).addHeader("Authorization", "Basic " + getBasicAuthMyCash());
    }

    public static void CancelRequestWS(Context _context,Boolean interruptIfRunning,Boolean isSync)
    {
        getClient(isSync).cancelRequests(_context, interruptIfRunning);
    }
    //----------------------------------------------------------------------------------------------------


    public static void sentListBiller(Context mContext, Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address List Biller:",LINK_LIST_BILLER);
        post(mContext,LINK_LIST_BILLER, params, responseHandler,isSync);
    }

    public static void sentDenomRetail(Context mContext, Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address Denom Retail:",LINK_DENOM_RETAIL);
        post(mContext,LINK_DENOM_RETAIL, params, responseHandler,isSync);
    }


    public static void sentCommAccountCollection(Context mContext, Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address sent comm account collect:",LINK_COMM_ACCOUNT_COLLECTION);
        post(mContext,LINK_COMM_ACCOUNT_COLLECTION, params, responseHandler,isSync);
    }

    public static void sentListBankBiller(Context mContext, Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address sent list bank biller:",LINK_LIST_BANK_BILLER);
        post(mContext,LINK_LIST_BANK_BILLER, params, responseHandler,isSync);
    }

    public static void getBillerType(Context mContext,Boolean isSync, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address Get Biller Type:",LINK_GET_BILLER_TYPE);
        get(mContext,LINK_GET_BILLER_TYPE, responseHandler, isSync);
    }

    public static void getGlobalBBSComm(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address global bbs comm:", LINK_GLOBAL_BBS_COMM);
        post(mContext, LINK_GLOBAL_BBS_COMM, params, responseHandler,isSync);
    }

    public static void getGlobalBBSAllComm(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address global bbs all comm:", LINK_GLOBAL_BBS_ALL_COMM);
        post(mContext, LINK_GLOBAL_BBS_ALL_COMM, params, responseHandler,isSync);
    }

    public static void getGlobalBBSBankC2A(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address global bbs bank C2A:",LINK_GLOBAL_BBS_BANK_C2A);
        post(mContext, LINK_GLOBAL_BBS_BANK_C2A, params, responseHandler, isSync);
    }

    public static void getGlobalBBSBankA2C(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address sent global bbs bank a2c:" , LINK_GLOBAL_BBS_BANK_A2C);
        post(mContext, LINK_GLOBAL_BBS_BANK_A2C, params, responseHandler,isSync);
    }

    public static void getListCommunity(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address sent list community:" , LINK_LISTCOMMUNITY_BBS);
        post(mContext, LINK_LISTCOMMUNITY_BBS, params, responseHandler,isSync);
    }

    public static void getListBankProductList(Context mContext,Boolean isSync, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.wtf("address sent list bank product bbs:" , LINK_BANK_PRODUCT_LIST);
        post(mContext, LINK_BANK_PRODUCT_LIST, params, responseHandler,isSync);
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

}
