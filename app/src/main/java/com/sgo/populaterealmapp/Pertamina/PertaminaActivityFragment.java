package com.sgo.populaterealmapp.Pertamina;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.Biller.PopulateService;
import com.sgo.populaterealmapp.R;
import com.sgo.populaterealmapp.ServiceReceiver;

/**
 * A placeholder fragment containing a simple view.
 */
public class PertaminaActivityFragment extends Fragment implements View.OnClickListener, ServiceReceiver.Receiver {

    private static final int PERMISSIONS_REQ_WRITEEXTERNALSTORAGE = 131;

    public static final int SCASH = 0x11;

    public static final int SCASHVerPROD = 0;

    public static final int SCASHVerDEV = 0;

    AlertDialog.Builder alertbox;
    ServiceReceiver resultReceiever;
    int button_id;
    ProgressBar mProgBar;
    TextView txt_pin;
    Switch sw_prod;
    int mProgress;


    public PertaminaActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //inisialisasi komponen di fragment pertamina
        View v = inflater.inflate(R.layout.fragment_pertamina, container, false);
        v.findViewById(R.id.button_pin).setOnClickListener(this);

        mProgBar = (ProgressBar)v.findViewById(R.id.progressBar);
        txt_pin = (TextView)v.findViewById(R.id.textView_pin);

        sw_prod = (Switch) v.findViewById(R.id.switch1);
        sw_prod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //jika 'checked' ubah variable headaddress ke prod lalu inisialisasi address-nya
                MyApiClient.IS_PROD = isChecked;
                if(MyApiClient.IS_PROD)
                    MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
                else
                    MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;
                MyApiClient.initializeAddress();
                Log.d("isi ISPROD", String.valueOf(MyApiClient.IS_PROD));
            }
        });
        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //inisialisasi serviceReceiver sebagai penerima hasil dari intent service
        resultReceiever = new ServiceReceiver(new Handler());
        resultReceiever.setReceiver(this);

        mProgress = 0;

        if(MyApiClient.IS_PROD)
            MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
        else
            MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;

        //tampilkan alertbox sebelum menjalankan service
        alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setTitle("Populate Realm");
        alertbox.setMessage("Apakah yakin membuat baru File Realm ini ?");
        alertbox.setPositiveButton("Ok", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        //tampilkan progressbar dan siapkan intent untuk memanggil service
                        mProgBar.setVisibility(View.VISIBLE);
                        mProgress = 0;
                        mProgBar.setProgress(mProgress);
                        Intent startIntent = new Intent(getActivity(),
                                PopulateService.class);

                        switch (button_id){
                            case R.id.button_pin :

                                //check jika prod ubah comm_id, address dan siapkan extra untuk spesifikasi prod
                                txt_pin.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_PIN;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.SCASH;
                                    startIntent.putExtra("realmName", "pinrealmdev.realm");
                                    startIntent.putExtra("realmVer",SCASHVerDEV);
                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_PIN;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.SCASH;
                                    startIntent.putExtra("realmName", "pinrealm.realm");
                                    startIntent.putExtra("realmVer",SCASHVerPROD);
                                }

                                startIntent.putExtra("appType",SCASH);
                                break;
                        }
                        MyApiClient.initializeAddress();
                        createTheRealm(startIntent);
                    }
                });
        alertbox.setNegativeButton("Cancel", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {}
                });



        mProgBar.setMax(100);
        mProgBar.setProgress(0);
        mProgBar.setIndeterminate(false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQ_WRITEEXTERNALSTORAGE);
        }
    }

    @Override
    public void onClick(View v) {
        button_id = v.getId();
        showDialog();

    }


    private void showDialog(){
        if(alertbox != null)
            alertbox.show();
    }

    private void createTheRealm(Intent startIntent){
        startIntent.putExtra("receiver", resultReceiever);
        getActivity().startService(startIntent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            //result yang diterima jika didapat result dari service yang dijalankan
            case PopulateService.DOWNLOAD_ERROR:
                Toast.makeText(getActivity(),resultData.getString("msg",""),Toast.LENGTH_SHORT).show();
                mProgBar.setVisibility(View.GONE);
                mProgBar.setProgress(0);
                break;
            case PopulateService.DOWNLOAD_SUCCESS:
                Toast.makeText(getActivity(),"Realm file selesai",Toast.LENGTH_SHORT).show();
                mProgBar.setProgress(mProgress+100);
                switch (resultData.getInt("appType")){
                    case SCASH :
                        txt_pin.setVisibility(View.VISIBLE);
                        break;
                }
                break;
            case PopulateService.DOWNLOAD_PROGRESS:
                if(mProgBar.getProgress() <= 98)
                    mProgBar.setProgress(++mProgress);
                break;
        }
    }

}
