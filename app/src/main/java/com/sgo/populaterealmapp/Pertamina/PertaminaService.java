package com.sgo.populaterealmapp.Pertamina;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sgo.populaterealmapp.Beans.Account_Collection_Model;
import com.sgo.populaterealmapp.Beans.Biller_Data_Model;
import com.sgo.populaterealmapp.Beans.Biller_Type_Data_Model;
import com.sgo.populaterealmapp.Biller.DateTimeFormat;
import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.WebParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by yuddistirakiki on 5/10/16.
 */
public class PertaminaService extends IntentService {

    public static final int DOWNLOAD_ERROR = 10;
    public static final int DOWNLOAD_SUCCESS = 11;
    public static final int DOWNLOAD_PROGRESS = 12;

    ResultReceiver receiver;
    Realm realm;
    Bundle oi;
    File folder;

    public PertaminaService() {
        super("PertaminaService");
    }

    public PertaminaService(String name) {
        super(name);
    }

    private void createFolder(){
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "PopulateRealmApp");
        if (!folder.exists()) {
            folder.mkdir();
        }

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        //membuat folder aplikasi jika belum ada
        createFolder();

        //inisialisasi bundle untuk dikirim lewat receiver
        oi = new Bundle();

        //inisialisasi realm dengan data dari intent yang dikirim
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(intent.getStringExtra("realmName"))
                .schemaVersion(intent.getIntExtra("realVer",0))
                .migration(new PertaminaRealMigration())
                .build();

        realm = Realm.getInstance(config);

        //ambil variable receiver dari extra intent
        receiver = intent.getParcelableExtra("receiver");
        getBiller();
    }

    public void getBiller(){
        try{

            receiver.send(DOWNLOAD_PROGRESS,null);
            MyApiClient.getBillerType(this, true,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response get Biller Type:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            String arrayBiller = response.getString(WebParams.BILLER_TYPE_DATA);
                            getDataCollection(new JSONArray(arrayBiller));
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi biller data list buy:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    public void getDataCollection(final JSONArray arrayBiller){
        try{

            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.CUSTOMER_ID, MyApiClient.USER_ID);
            params.put(WebParams.USER_ID, MyApiClient.USER_ID);
            params.put(WebParams.DATETIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Log.d("Isi params CommAccountCollection:" , params.toString());

            MyApiClient.sentCommAccountCollection(this,true,params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        Log.d("Isi response CommAccountCollection:" , response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE) || code.equals("0003")) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            JSONArray arrayCollection ;
                            String data = response.getString(WebParams.COMMUNITY);
                            if(code.equals("0003")||data.equals("")){
                                arrayCollection = new JSONArray();
                            }
                            else
                                arrayCollection = new JSONArray(data);
                            initializeData(arrayBiller,arrayCollection);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",message);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    Log.w("Error Koneksi collect data list buy:",throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:" , e.getMessage());
        }
    }


    private void initializeData(final JSONArray arrayBiller, final JSONArray arrayCollection){
        receiver.send(DOWNLOAD_PROGRESS,null);
        final String curr_date = DateTimeFormat.getCurrentDate();

        Biller_Type_Data_Model mBillerObj;
        RealmResults<Biller_Type_Data_Model> ResultData = realm.where(Biller_Type_Data_Model.class).findAll();
        List<Biller_Type_Data_Model> temptData = realm.copyFromRealm(ResultData);

        realm.beginTransaction();

        if (arrayBiller.length() > 0) {
            receiver.send(DOWNLOAD_PROGRESS,null);
            realm.delete(Biller_Type_Data_Model.class);
            Biller_Data_Model mListBillerData;
            int i;
            int j;
            int k;
            for ( i= 0; i < arrayBiller.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                try {
                    mBillerObj = realm.createObjectFromJson(Biller_Type_Data_Model.class, arrayBiller.getJSONObject(i));
                    mBillerObj.setLast_update(curr_date);

                    for (j = 0; j < temptData.size(); j++) {
                        receiver.send(DOWNLOAD_PROGRESS,null);
                        if (mBillerObj.getBiller_type_code().equals(temptData.get(j).getBiller_type_code())) {
                            for ( k = 0; k < temptData.get(j).getBiller_data_models().size(); k++) {
                                mListBillerData = realm.where(Biller_Data_Model.class).
                                        equalTo("comm_id", temptData.get(j).getBiller_data_models().get(k).getComm_id()).
                                        equalTo("comm_name", temptData.get(j).getBiller_data_models().get(k).getComm_name()).
                                        findFirst();
                                mBillerObj.getBiller_data_models().add(mListBillerData);
                            }
                            break;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Boolean isHave = false;
            Biller_Data_Model delList;
            for(i = 0 ; i < temptData.size() ; i++){
                for (j = 0 ; j < ResultData.size() ; j++){
                    if(temptData.get(i).getBiller_type_code().equals(ResultData.get(j).getBiller_type_code())){
                        isHave = true;
                    }
                }

                receiver.send(DOWNLOAD_PROGRESS,null);
                if(!isHave){
                    for ( j = 0; j < temptData.get(i).getBiller_data_models().size(); j++) {
                        delList = realm.where(Biller_Data_Model.class).
                                equalTo("comm_id", temptData.get(i).getBiller_data_models().get(j).getComm_id()).
                                equalTo("comm_name", temptData.get(i).getBiller_data_models().get(j).getComm_name()).
                                findFirst();
                        if(delList.getDenom_data_models().size()>0)
                            delList.getDenom_data_models().deleteAllFromRealm();
                        delList.deleteFromRealm();
                    }
                }
                isHave = false;
            }
        }

        receiver.send(DOWNLOAD_PROGRESS,null);

        if (arrayCollection.length() > 0) {
            realm.delete(Account_Collection_Model.class);
            Account_Collection_Model mACLobj;
            try {
                for (int i = 0; i < arrayCollection.length(); i++) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    mACLobj = realm.createObjectFromJson(Account_Collection_Model.class, arrayCollection.getJSONObject(i));
                    mACLobj.setLast_update(curr_date);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        realm.commitTransaction();

        EndError(DOWNLOAD_SUCCESS,oi);
    }


    private void EndRealm(){
        if(realm.isInTransaction())
            realm.cancelTransaction();

        if(realm != null && !realm.isClosed())
            realm.close();
    }

    private void EndError(int ReceiverTag, Bundle io){
        Bundle buk = (Bundle)io.clone();
        io.clear();
        EndRealm();
        receiver.send(ReceiverTag,buk);

    }
}
