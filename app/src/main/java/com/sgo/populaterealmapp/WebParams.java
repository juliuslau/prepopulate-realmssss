package com.sgo.populaterealmapp;/*
  Created by Administrator on 1/6/2015.
 */

public class WebParams {

    public static String SUCCESS_CODE= "0000";
    public static String SATU = "1";
    public static String LOGOUT_CODE= "0404";

    public static String ERROR_CODE= "error_code";
    public static String ERROR_MESSAGE= "error_message";
    public static String MESSAGE= "message";
    public static String DATE_TIME= "datetime";

    public static String COMM_ID= "comm_id";
    public static String CUST_NAME = "cust_name";
    public static String CUST_ID = "cust_id";
    public static String COMM_CODE = "comm_code";
    public static String USER_COMM_CODE = "user_comm_code";
    public static String USER_ID = "user_id";
    public static String TOKEN_ID = "token_id";
    public static String MEMBER_CODE = "member_code";
    public static String CUSTOMER_ID = "customer_id";

    public static String CUST_PHONE = "cust_phone";
    public static String CUST_EMAIL = "cust_email";
    public static String MAX_RESEND_TOKEN = "max_resend_token";
    public static String MAX_RESEND = "max_resend";
    public static String COUNT_RESEND = "count_resend";

    public static String PASSWORD_LOGIN = "password";
    public static String USER_NAME = "user_name";
    public static String COMMUNITY= "community";
    public static String BUSS_SCHEME_NAME= "buss_scheme_name";
    public static String BUSS_SCHEME_CODE= "buss_scheme_code";
    public static String COMM_NAME = "comm_name";
    public static String CALLBACK_URL = "callback_url";

    public static String MEMBER_DATA = "member_data";
    public static String MEMBER_ID = "member_id";
    public static String MEMBER_NAME = "member_name";

    public static String CCY_ID = "ccy_id";
    public static String AMOUNT = "amount";

    public static String BANK_BILLER = "bank_biller";
    public static String BANK_DATA = "bank_data";
    public static String BANK_NAME = "bank_name";
    public static String BANK_CODE = "bank_code";
    public static String PRODUCT_NAME = "product_name";
    public static String PRODUCT_TYPE = "product_type";
    public static String PRODUCT_CODE = "product_code";
    public static String PAYMENT_REMARK = "payment_remark";
    public static String PRODUCT_H2H = "product_h2h";

    public static String TX_ID = "tx_id";
    public static String PRODUCT_VALUE = "product_value";

    public static String OLD_PASSWORD = "old_password";
    public static String NEW_PASSWORD = "new_password";

    public static String OLD_PIN = "old_pin";
    public static String NEW_PIN = "new_pin";
    public static String CONFIRM_PIN = "confirm_pin";


    public static String MERCHANT_CODE = "merchant_code";
    public static String MERCHANT_NAME = "merchant_name";

    public static String DOB = "date_of_birth";
    public static String SOCIAL_ID = "social_id";
    public static String COUNTRY = "country";
    public static String BIO = "bio";
    public static String ADDRESS = "address";
    public static String EMAIL = "email";
    public static String FULL_NAME = "full_name";
    public static String HOBBY = "hobby";
    public static String POB = "birth_place";
    public static String ID_TYPE = "idtype";
    public static String VERIFIED = "verified";
    public static String MOTHER_NAME = "mother_name";

    public static String API_KEY = "api_key";

    public static String USER_IS_NEW = "user_is_new";
    public static String CONTACTS = "contacts";

    public static String BILLER_TYPE_DATA = "biller_type_data";
    public static String BILLER_TYPE_CODE = "biller_type_code";
    public static String BILLER_TYPE_ID = "biller_type_id";
    public static String BILLER_TYPE_NAME = "biller_type_name";

    public static String BILLER_TYPE = "biller_type";
    public static String BILLER_DATA = "biller_data";
    public static String BILLER_ID = "biller_id";
    public static String BILLER_CODE = "biller_code";
    public static String BILLER_NAME = "biller_name";
    public static String BILLER_INPUT_AMOUNT = "biller_input_amount";
    public static String BILLER_DISPLAY_AMOUNT = "biller_display_amount";
    public static String DESCRIPTION = "description";

    public static String MEMBER_CUST = "member_cust";

    public static String DENOM_DATA = "denom_data";
    public static String DENOM_ITEM_ID = "item_id";
    public static String DENOM_ITEM_NAME = "item_name";
    public static String DENOM_ITEM_PRICE = "item_price";
    public static String DENOM_CCY_ID = "ccy_id";
    public static String DENOM_ITEM_REMARK = "item_remark";

    public static String TX_STATUS = "tx_status";
    public static String TX_REMARK = "tx_remark";

    public static String IMG_URL = "img_url";
    public static String IMG_SMALL_URL = "img_small_url";
    public static String IMG_MEDIUM_URL = "img_medium_url";
    public static String IMG_LARGE_URL = "img_large_url";

    public static String MEMBER_REMARK = "member_remark";
    public static String DATA = "data";
    public static String MEMBER_CODE_TO = "member_code_to";
    public static String DATA_TRANSFER = "data_transfer";
    public static String MEMBER_NAME_TO = "member_name_to";
    public static String MEMBER_PHONE = "member_phone";
    public static String MEMBER_STATUS = "member_status";

    public static String FEE = "fee";
    public static String TOTAL = "total";
	public static String DESC = "desc";
    public static String REQUEST_ID = "request_id";
    public static String SEND_TO = "send_to";

    public static String DATA_CONTACT = "data_contact";
    public static String DATA_FRIEND = "data_friend";

    public static String NOTIF_ID = "id";
    public static String NOTIF_ID_READ = "notif_id";
    public static String NOTIF_TYPE = "notif_type";
    public static String FROM_NAME = "from_name";
    public static String NOTIF_READ = "read";
    public static String NOTIF_DATA = "data_user_notif";
    public static String NOTIF_DETAIL = "notif_detail";

    public static String DATA_GROUP = "data_group";
    public static String COUNT = "count";
    public static String GROUP_ID = "group_id";
    public static String GROUP_NAME = "group_name";
    public static String GROUP_DESC = "group_desc";
    public static String OWNER_ID = "owner_id";
    public static String CREATED= "created";
    public static String CREATED_DATE = "created_date";
    public static String UPDATED_DATE ="updated_date";

    public static String MEMBERS = "members";
    public static String APP_ID = "app_id";
    public static String ACTIVE = "active";
    public static String MEMBER_PROFILE_PICTURE = "member_profile_picture";

    public static String DATA_POSTS = "data_posts";
    public static String ID = "id";
    public static String POST = "post";
    public static String BALANCE = "balance";
    public static String DATETIME = "datetime";
    public static String OWNER = "owner";
    public static String OWNER_PROFILE_PICTURE = "owner_profile_picture";
    public static String WITH_ID = "with_id";
    public static String WITH = "with";
    public static String WITH_PROFILE_PICTURE = "with_profile_picture";
    public static String TYPEPOST = "typepost";
    public static String TYPECAPTION = "typecaption";
    public static String NUMCOMMENTS = "numcomments";
    public static String NUMVIEWS = "numviews";
    public static String NUMLIKES = "numlikes";
    public static String SHARE = "share";
    public static String COMMENTS = "comments";
    public static String LIKES = "likes";

    public static String DATA_COMMENTS = "data_comments";
    public static String DATA_LIKES = "data_likes";
    public static String COMMENT_ID = "comment_id";
    public static String LIKE_ID = "like_id";
    public static String POST_ID = "post_id";
    public static String FROM = "from";
    public static String FROM_PROFILE_PICTURE = "from_profile_picture";
    public static String TO = "to";
    public static String TO_NAME = "to_name";
    public static String TO_PROFILE_PICTURE = "to_profile_picture";
    public static String REPLY = "reply";

    public static String NO_DATA_CODE= "0003";
	public static String TRX_ID = "trx_id";
    public static String TX_TYPE = "tx_type";
    public static String STATUS = "status";
    public static String UNREAD = "unread";

    public static String PRIVACY = "privacy";
    public static String DATA_MAPPER = "data_mapper";

	public static String AUTHENTICATION_TYPE = "authentication_type";
    public static String AUTH_TYPE = "auth_type";

    public static String LENGTH_AUTH = "length_auth";
    public static String IS_HAVE_PIN = "is_have_pin";

    public static String REMAIN_LIMIT = "remain_limit";
    public static String PERIOD_LIMIT = "period_limit";
    public static String NEXT_RESET = "next_reset";

    public static String PAGE= "page";
    public static String DATE_FROM= "date_from";
    public static String DATE_TO= "date_to";
    public static String REPORT_DATA= "report_data";
    public static String TYPE= "type";
    public static String REMARK= "remark";
    public static String DETAIL= "detail";
    public static String NEXT= "next";

    public static String MIN_PWD= "min_pwd";
    public static String MAX_PWD= "max_pwd";

    public static String BILLER_PARENT_COMM_ID= "biller_parent_comm_id";
    public static String BILLER_PARENT_COMM_CODE= "biller_parent_comm_code";
    public static String BILLER_PARENT_API_KEY= "biller_parent_api_key";

	public static String PROMO_DATA= "promo_data";
    public static String NAME = "name";
    public static String BANNER_PIC = "banner_pic";
    public static String TARGET_URL = "target_url";
    public static String ATM_TOPUP_DATA = "atm_topup_data";
    public static String NO_VA = "no_va";

    public static String MERCHANT_TYPE = "merchant_type";

	public static String APP_DATA = "app_data";
    public static String PACKAGE_NAME = "package_name";
    public static String PACKAGE_VERSION = "package_version";

    public static String CONTACT_DATA = "contact_data";
    public static String CONTACT_PHONE = "contact_phone";
    public static String CONTACT_EMAIL = "contact_email";

    public static String NO_HP = "no_hp";
    public static String ACCT_NO = "acct_no";
    public static String ACCT_NAME = "acct_name";
    public static String TGL_LAHIR = "tgl_lahir";

    public static String DOWNLOAD_URL = "download_url";
    public static String SETTINGS = "settings";
    public static String MAX_MEMBER_TRANSFER = "max_member_transfer";
    public static String ADMIN_FEE = "admin_fee";

    public static String ACCOUNT_DATA = "account_data";
    public static String ACCOUNT_NO = "account_no";
    public static String ACCOUNT_NAME = "account_name";
    public static String ACCOUNT_TYPE = "account_type";

    public static String FAILED_ATTEMPT = "failed_attempt";
    public static String MAX_FAILED = "max_failed";

    public static String VALIDATE_ID = "validate_id";
    public static String GENDER = "gender";
    public static String LANGUAGE = "language";
    public static String LOCATION = "location";
    public static String LOCALE = "locale";
    public static String PROFILE_URL = "profile_url";
    public static String FACEBOOK_CONNECT = "fb_connect";

    public static String SOCIAL_SIGNATURE = "social_signature";
    public static String RECIPIENT_NAME = "recipient_name";

	public static String RC_UUID = "rc_uuid";
    public static String RC_DTIME = "rc_dtime";
    public static String SIGNATURE = "signature";

    public static String ACCESS_KEY = "access_key";
    public static String ACCESS_SECRET = "access_secret";

    public static String USER_FILE = "userfile";

    public static String TO_ALIAS = "to_alias";

	public static String REASON = "reason";
    public static String PASS = "pass";
    public static String CONF_PASS = "conf_pass";
    public static String PIN = "pin";
    public static String CONF_PIN = "conf_pin";
    public static String IS_SMS = "is_sms";
    public static String IS_EMAIL = "is_email";

    public static String CUST_ID_TYPE = "cust_id_type";
    public static String CUST_ID_NUMBER = "cust_id_number";
    public static String CUST_ADDRESS = "cust_address";
    public static String CUST_COUNTRY = "cust_country";
    public static String CUST_BIRTH_PLACE = "cust_birth_place";
    public static String CUST_BIRTH_DATE = "cust_birth_date";
    public static String CUST_MOTHER_NAME = "cust_mother_name";
    public static String CUST_GENDER = "cust_gender";
    public static String ID_TYPES = "id_types";

    public static String MEMBER_LEVEL = "member_level";
    public static String IS_REGISTERED = "is_registered";

	public static String BANK_CASHOUT = "bank_cashout";
    public static String USER_IS_SYNCED = "user_is_synced";

    public static String FROM_USER_ID = "from_user_id";
    public static String TO_USER_ID = "to_user_id";

    public static String CONTACT_CENTER = "contact_center";
    public static String CHANGE_PASS = "changed_pass";
    public static String USERID_ADMIN = "user_id_admin";
    public static String NAME_ADMIN = "user_name_admin";
    public static String OTP_MEMBER = "otp_member";
    public static String CUST_CONTACT_EMAIL = "cust_contact_email";
    public static String MAX_TOPUP = "max_topup";
    public static String IS_REGISTER = "is_register";
    public static String ALLOW_MEMBER_LEVEL = "allow_member_level";
    public static String CAN_TRANSFER = "can_transfer";

    public static String DEV_MODEL = "dev_model";
    public static String MAC_ADDR = "mac_addr";
    public static String BANK_GATEWAY = "bank_gateway";

    public static String UNREAD_NOTIF = "unread_notif";
    public static String SCHEME_CODE= "scheme_code";
    public static String COMM_ID_PERTA = "comm_id_perta";
    public static String COMM_SOURCE = "comm_source";
    public static String COMM_BENEF = "comm_benef";
    public static String COMM_ID_REMARK = "comm_id_remark";

}
